//Cargamos la página
window.addEventListener('load', function() {
    // 'Bindeamos' los botones
    var btnLoadData = document.getElementById('btnMov');
    var btnLoadAllData = document.getElementById('btnAllMov');

    // Variable para realizar la petición
    const xhr = new XMLHttpRequest();
    //Variable para mostrar futuramente los datos
    var output = "";


    //Realizamos los eventos
    //Carga de un movimimiento
    btnLoadData.addEventListener('click', function() {
        output = "";
        fetch('./JSONFiles/latestMov.json')
            .then(function(res) {
                return res.json();
            })
            .then(function(data) {
                var account = data;
                output = `<div id="table-result" class="table-responsive container container-fluid">
                <div id="dateField" class="row container-fluid table-primary "><h4>DATOS</h4></div>
                <!--Fecha-->
                <div id="dateField" class="row container-fluid">
                    <div class="col">
                        <strong>Fecha</strong>
                    </div>
                    <div class="col">
                        ${account.date}
                    </div>
                </div>
                <!--Concepto-->
                <div id="conceptField" class="row container-fluid">
                    <div class="col">
                    <strong>Concepto</strong>
                    </div>
                    <div class="col">
                        ${account.concept}
                    </div>
                </div>
                <!--Cantidad-->
                <div id="amountField" class="row container-fluid">
                    <div class="col">
                    <strong>Cantidad</strong>
                    </div>
                    <div class="col">
                        ${account.amount}
                    </div>
                </div>
            </div>`;
                document.getElementById('result-container').innerHTML = output;
            })
            .catch(function(err) {
                output = `<h3>ERROR, No pudo realizarse correctamente</h3>`;
                document.getElementById('result-container').innerHTML = output;
            });
    });

    //------
    //Cargar todos los movimientos
    btnLoadAllData.addEventListener('click', function() {
        output = "";
        fetch('./JSONFiles/allMoves.json')
            .then(function(res) {
                return res.json();
            })
            .then(function(data) {
                const movements = data;
                //Por cada movimiento
                movements.forEach(function(movement) {
                    output += `<!--Fecha-->
                    <div id="dateField" class="row container-fluid table-secondary"><h6>Movimiento ${movement.date}</h6></div>
                    <div id="dateField" class="row container-fluid">
                        <div class="col">
                            <strong>Fecha</strong>
                        </div>
                        <div class="col">
                            ${movement.date}
                        </div>
                    </div>
                    <!--Concepto-->
                    <div id="conceptField" class="row container-fluid">
                        <div class="col">
                        <strong>Concepto</strong>
                        </div>
                        <div class="col">
                            ${movement.concept}
                        </div>
                    </div>
                    <!--Cantidad-->
                    <div id="amountField" class="row container-fluid">
                        <div class="col">
                        <strong>Cantidad</strong>
                        </div>
                        <div class="col">
                            ${movement.amount}
                        </div>
                    </div>`;
                });
                output += `</div>`;
                document.getElementById('result-container').innerHTML = output;
            })
            .catch(function(err) {
                output = `<h3>ERROR, No pudo realizarse correctamente</h3>`;
                document.getElementById('result-container').innerHTML = output;
            });
    });

});